---
title: "Untitled"
author: "Charles"
date: "September 27, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Librairies
 
```{r, include=FALSE}

GeneralHelpers::loadAll()
library(tidyverse)
library(magrittr)
library(ggplot2)
 
 
library(sf)
library(leaflet)

library(here)
library(glue)

library(purrr)

```


#Read the data

##Shp files
```{r}

shpQcNeigh <- st_read(here("Data","GeoData","Neighbourhoods" ))  %>% st_zm
shpMontcalm <- shpQcNeigh %>% filter(NOM == "Montcalm")

shpStreets <-  rgdal::readOGR(dsn=here("Data","GeoData","PlainsAbraham" ), layer="streetsLines") %>% st_as_sf() %>% st_zm
shpParcBraves <- rgdal::readOGR(dsn=here("Data","GeoData","PlainsAbraham" ), layer="parcBravesPolyV2") %>% st_as_sf() %>% st_zm
shpWolfe <- rgdal::readOGR(dsn=here("Data","GeoData","PlainsAbraham" ), layer="wolfeMemorialPoint") %>% st_as_sf() %>% st_zm
shpPlains <- rgdal::readOGR(dsn=here("Data","GeoData","PlainsAbraham" ), layer="plainsAbrahamPolyV3") %>% st_as_sf() %>% st_zm


```

##Csv
###Streets
```{r}

dfTopo <- read_csv(here("Data","Csv","topoBattle.csv"))

dfTopo %>% dim

dfTopo$nom
```

###Parcs
```{r}

dfTopoParc <- read_csv(here("Data","Csv","topoParcs.csv"))

dfTopoParc %>% dim

dfTopoParc$nom
```

###Neigh
```{r}

dfTopoNeigh<- read_csv(here("Data","Csv","topoNeigh.csv") )

dfTopoNeigh %>% dim

dfTopoNeigh$nom
```

##Merge street info

###Remove the Avenue

```{r}

streetStrPatt <- "[rR]uelle|[rR]ue|[aA]venue|[bB]oulevard|[cC]ôte|[pP]ente|[cC]arrefour|[aA]llé|[cC]hemin|[pP]assage|[eE]scalier|[cC]haussée|[rR]oute|[pP]romenade|[iI]mpasse|[cC]ul-de-sac|[mM]ontée"

streetStrPattLookBehind <- paste0("(?<=", streetStrPatt, ")[ ].+")

listNames <- list()
for(k in 1:nrow(shpStreets)){
  
  strEntireName <- shpStreets$name[[k]]
  res <- regexpr( streetStrPattLookBehind, strEntireName, perl=T)
  start <- res
  end <- res+attr(res,"match.length")-1
  
  newS <- substr(strEntireName, start+1,end) #for the spae
  newSBis <- GeneralHelpers::makeFirstLetterUpper(newS)
  
  listNames <- rlist::list.append(listNames,  newSBis)
}

shpStreets$namesShort <- unlist(listNames) %>% as.character()

```

###Actual merge on new column
```{r}

shpStreetsWithInfo <- left_join( shpStreets, dfTopo, by=c("namesShort"="nom"))

shpStreetsWithInfo$info %>% is.na %>% sum()

shpStreetsWithInfo %>% filter(is.na(info)) %>% pull(name)

stopifnot(shpStreetsWithInfo$info %>% is.na %>% sum() == 0)
```

```{r}

shpParcBravesWithInfo <- left_join( shpParcBraves , dfTopoParc, by=c("name"="nom"))
shpPlainsWithInfo <- left_join( shpPlains , dfTopoParc, by=c("name"="nom"))

```

```{r}

shpMontcalmWithInfo <- left_join( shpMontcalm , dfTopoNeigh, by=c("NOM"="nom"))
 
```

###Create the popupstr
```{r}

popupStr <- paste0("<b>Street name: </b>", shpStreetsWithInfo$name ,  '<br>',
                   "<b>Description :</b>" ,shpStreetsWithInfo$info  )

popupStrParcBraves <- paste0("<b>Parc name: </b>", shpParcBravesWithInfo$name ,  '<br>',
                   "<b>Description :</b>" ,shpParcBravesWithInfo$info  )

popupStrParcPlains <- paste0("<b>Parc name: </b>", shpPlainsWithInfo$name ,  '<br>',
                   "<b>Description :</b>" ,shpPlainsWithInfo$info  )

popupStrNeigh <- paste0("<b>Parc name: </b>", shpMontcalmWithInfo$NOM ,  '<br>',
                   "<b>Description :</b>" ,shpMontcalmWithInfo$info  )
  
   
```

###Set the hover label
```{r}

shpWolfe$name <- "Wolfe memorial" 
   
shpMontcalmBoundary <- shpMontcalm %>% st_boundary()
shpMontcalmBoundary$name <- "Montcalm"

```

#Ggplot
```{r}

ggplot() + 
  geom_sf(data=shpMontcalm %>% st_boundary()) + 
  geom_sf(data=shpStreets, col="blue") + 
  geom_sf(data=shpWolfe, col="green", size=3) +
  geom_sf(data=shpParcBraves , col="red", alpha=0.4)+ 
  geom_sf(data=shpPlains , col="orange", alpha=0.4)


```


#Leaflet
```{r}

plainsLeaflet <- leaflet() %>% 
  addTiles() %>% 
  #
  addMapPane("shpMontcalm", zIndex = 200) %>% #the tiles get a zindex 
  addMapPane("shpPlaines", zIndex = 300) %>%
  addMapPane("shpStreets", zIndex = 400) %>% #higher z-index means higher priority (upper layer in map)
  #
  #Montcalm (backgroup - lower zindex)
  addPolygons(data=shpMontcalmBoundary,  popup=popupStrNeigh, col="black", opacity =0.2 , options=pathOptions(pane="shpMontcalm"),label=~name) %>% 
  #Braves + plaines
  addPolygons(data=shpParcBraves,popup = popupStrParcBraves, col="red",options=pathOptions(pane="shpPlaines"), label = ~name) %>% 
  addPolygons(data=shpPlains,popup = popupStrParcPlains , col="orange",options=pathOptions(pane="shpPlaines"), label = ~name) %>% 
  #Streets + memorial
  addPolylines(data=shpStreets , popup = ~popupStr, col="blue", options=pathOptions(pane="shpStreets"), label = ~name) %>% 
  addCircles(data=shpWolfe , popup ="Wolfe memorial", col="green", options=pathOptions(pane="shpStreets"), label = ~name) 
 


 
htmlwidgets::saveWidget(widget = plainsLeaflet,
                        file= here("Html","plainesMaps.html"),
                        selfcontained=F)
 

(plainsLeaflet)
```