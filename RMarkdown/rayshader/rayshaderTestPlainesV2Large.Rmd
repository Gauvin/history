---
title: "rayshaderPlainesV2"
author: "Charles"
date: "July 3, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Librairies
 
```{r, include=FALSE}

library(tidyverse)
library(magrittr)
library(ggplot2)
library(mapdeck)
library(rayshader)
library(rgdal)
library(raster)
library(sf)
library(leaflet)
library(rayshaderDemo)
```
 
#Initial elevation raster 
##Load raster of elevation
```{r}

miceadds::load.Rdata(filename = here::here("Data","Rdata","qcRasterElevationFromBbox_14.Rdata"),
                     objname = "qcRasterElevationFromBbox_14")

```


##Get a smaller area first (covers parts of qc, levis, beauport and iles d'orleans)
```{r}


raster <- raster::crop(
  qcRasterElevationFromBbox_14,
  raster::extent(c(-71.26723153291927,-71.1205323212404,46.78595770364773,46.88154140875724))
)

raster %>% plot


```

 

#Load raster of topo map
 
## Elevation

```{r}

#Need elev raster to build the elevation matrix
dirFile <- here::here("Figures", "Raster")
fileName <- "qcMapFromRasterLarger.tif"
elev_file_name_str_abs <- file.path(dirFile,  fileName)
  
 if(file.exists(elev_file_name_str_abs)){
   file.remove(elev_file_name_str_abs)
 }
```


##Load an overlay map from a given raster (using the bbox)

```{r}
create_overlay_map_arcgis <- function(rastElev,
                                             overlay_file_name =  "qcTopoMapOverlay.png" ,
                                             overlay_file_dir = here::here("images", "rayshader", "test"), 
                                             map_type= "World_Topo_Map",
                                      forceOverwrite=T){
  
  #Check that we really have a RasterLayerr object - NOT a matrix since we need to get the bbox
  stopifnot(
    any(  class(rastElev) %in% c("RasterLayer" ) )
  )
  
  bbox <- getBboxFromRaster(rastElev)
   
  image_size <- define_image_size(bbox, max(dim(rastElev)))

  #Downdload the image if necessary
  fileAbsPath <- file.path(overlay_file_dir, overlay_file_name)
  if(!file.exists(fileAbsPath) || forceOverwrite){
    
    print(paste0("in create_overlay_map_arcgis => overlay does NOT exist in ", 
                 fileAbsPath, " => creating it"))
    
    get_arcgis_map_image(bbox, 
                         map_type = map_type, 
                         file = fileAbsPath,
                         width = image_size$height,
                         height = image_size$height, 
                         sr_bbox = 4326
    )
  }else{
    print(paste0("in create_overlay_map_arcgis => overlay exists in ", 
                 fileAbsPath ))
  }
  
  
  
  overlay_img <- png::readPNG(fileAbsPath)
  
  return(overlay_img)
}
```

```{r}



overlay_img <- create_overlay_map_arcgis(rastElev = raster,
                                         overlay_file_dir = dirFile,
                                         overlay_file_name = fileName,map_type = "World_Topo_Map"
)

imager::as.cimg(overlay_img) %>% plot()
```
##Convert the raster to a matrix with appropriate dimensions
```{r}

rasterResampled <- raster::resample(raster,
         raster::raster( ncol=ncol(overlay_img), 
                         nrow=nrow(overlay_img)
         ),
         method="ngb"
        )

rasterResampled %>% plot

matElev <- matrix(rasterResampled %>% values, 
                  ncol=nrow(overlay_img), 
                  nrow=ncol(overlay_img), 
                  byrow = F) 
 

```

##Compare dimensions
```{r}

print(dim(overlay_img))
print(dim(matElev))

stopifnot(dim(overlay_img)[1] == dim(matElev)[2] && 
            dim(overlay_img)[2] == dim(matElev)[1] )
```
 

```{r}

bbox <- getBboxFromRaster(raster)
image_size <- define_image_size(bbox = bbox,
                                major_dim = max(dim(matElev)) )

```

 
#Labels for important areas 
```{r}

listLabels <- list()

labelCitadel <- list(text = "Citadel")
labelCitadel$pos <- find_image_coordinates(
  long = -71.207373, 
  lat = 46.80736,  
  bbox = bbox,
  image_width = image_size$width, 
  image_height = image_size$height)
listLabels[[1]] <- labelCitadel

labelLevis <- list(text = "Pointe-Lévis")
labelLevis$pos <- find_image_coordinates(
  long = -71.166966, 
  lat = 46.828649,  
  bbox = bbox,
  image_width = image_size$width, 
  image_height = image_size$height)
listLabels[[2]] <- labelLevis
 
labelIlesOrleans <- list(text = "Île-d'Orléans")
labelIlesOrleans$pos <- find_image_coordinates(
  long = -71.137657, 
  lat = 46.848961,  
  bbox = bbox,
  image_width = image_size$width, 
  image_height = image_size$height)

listLabels[[3]] <- labelIlesOrleans


labelBeauport <- list(text = "Beauport")
labelBeauport$pos <- find_image_coordinates(
  long = -71.160733, 
  lat = 46.868043,  
  bbox = bbox,
  image_width = image_size$width, 
  image_height = image_size$height)

listLabels[[4]] <- labelBeauport

 
```
  


#Raytracing

##Plot the elevation in 3D
```{r}
 

 matElev %>% 
  sphere_shade() %>% 
  add_overlay( overlay_img, alphalayer = 0.5) %>% 
  plot_3d(matElev,zoom=0.5)  

```

##Render labels
```{r}

for( k in 1:length(listLabels)){
  render_label(matElev, 
               x = listLabels[[k]]$pos$x, 
               y = listLabels[[k]]$pos$y, 
               z = 100, 
               text = listLabels[[k]]$text, 
               textsize = 1,
               linewidth = 2) 
}


```
 

```{r}

snapshotPngFilePath <- here::here("Figures","Rayshader","qcBattleFieldsV2Larger.png")
 if(file.exists(snapshotPngFilePath)){
   file.remove(snapshotPngFilePath)
 }
render_snapshot(snapshotPngFilePath)

```

 